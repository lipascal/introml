# Intro to ML

**Prerequisits**: Anaconda 

## Environment (same as for live demos)
First check if you have already created it:
```bash
$ conda env list
```
If the output lists intro-ml go to **Install requirements**. 
You have already created it.
```bash
conda environments:

base                  *  /home/pascal/anaconda3
intro-ml                 /home/pascal/anaconda3/envs/intro-ml
```

Create a conda environment where everything should run. 
```bash
$ conda create -n intro-ml python=3.7 anaconda
$ conda activate intro-ml
```

## Install requirements
To install the requirements. Installs packages like pandas etc.  
*If you add an additional library to your conda environment,  
please add it to the requirements.txt file*
```bash
$ pip install -r requirements.txt
```

## Run a jupyter notebook server
Start a jupyter notebook server by running 
```bash
$ jupyter notebook 
```
